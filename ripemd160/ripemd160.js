module.exports = function (RED) {
  const NodeName = 'hash ripemd160'

  function Ripemd160HashNode (config) {
    RED.nodes.createNode(this, config)
    let crypto
    try {
      crypto = require('crypto')
    } catch (err) {
      console.log('Crypto support is disabled!')
    }
    const algorithm = 'ripemd160'

    function hash (text) {
      const hash = crypto.createHash(algorithm)
      hash.update(text)
      return hash.digest('hex')
    }

    const node = this
    this.on('input', function (msg) {
      msg.payload = hash(msg.payload)
      node.send(msg)
    })
  }
  RED.nodes.registerType(NodeName, Ripemd160HashNode)
}
