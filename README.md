# node-red-contrib-cryptography

[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)

Simple NodeRed Node for cryptography. 

Simple hash with AES-256 and RIPEMD-160

A sample Flow:

![image](http://blog.wenzlaff.de/wp-content/uploads/2021/02/hash-flow.png)

More for this Node-Red Node on http://blog.wenzlaff.de/?p=16346

# Install
npm install node-red-contrib-cryptography

# Sample
Input String in msg.payload: "Der Text von dem der Hash gebildet wird. Er soll auch mal ein paar Umlaute enthalten wie öü oder ÖÜ"

Output Hash with AES-256 in Hex: "9307f6730bb57a068c3662c775649c4fbadfb0ddf3d11c43e525259a43c1a150"

A sample Flow is in the **/examples** Dir.

It is a wrapper for Node.js crypto Lib from https://nodejs.org/api/crypto.html

# Sample Flow SHA-256

![image](http://blog.wenzlaff.de/wp-content/uploads/2021/02/nodered-flow-hash.png)

NodeRed Gui:

![image](http://blog.wenzlaff.de/wp-content/uploads/2021/02/sha-256.png)

# Sample Flow RIPEMD-160

![image](http://blog.wenzlaff.de/wp-content/uploads/2021/02/nodered-ripemd-160.png)

NodeRed Gui:

![image](http://blog.wenzlaff.de/wp-content/uploads/2021/02/wenzlaff.de-2021-02-22-um-19.28.21.png)



